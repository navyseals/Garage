package com.garage.acedetails;

import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.password.PasswordEncoder;
import com.garage.acedetails.service.ImageService;

@SpringBootApplication
//@ComponentScan("com.example")
public class GarageApplication implements CommandLineRunner {
  
//  @Resource
//  ImageService imageService;

  @Autowired
  private ApplicationContext applicationContext;

  public static void main(String[] args) {
    SpringApplication.run(GarageApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    //imageService.init();
  }

}
