package com.garage.acedetails.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.garage.acedetails.dto.AccountDto;
import com.garage.acedetails.entity.Account;
import com.garage.acedetails.model.DataResponse;
import com.garage.acedetails.service.AccountService;
import com.garage.acedetails.util.UserRole;

@RequestMapping("/api/v1")
@RestController
public class AccountController {
  @Autowired
  private AccountService accountService;

  @PostMapping("/account/add-user")
  // Everyone has the right to add users
  public ResponseEntity<DataResponse> insertUser(@RequestBody AccountDto accountDto) {
    Account account = accountService.insertAccount(accountDto, UserRole.ROLE_USER);
    if (account != null) {
      return ResponseEntity.status(HttpStatus.OK).body(new DataResponse(true));
    } else {
      DataResponse dataResponse = DataResponse.BAD_REQUEST;
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(dataResponse);
    }
  }

  @PostMapping("/account/add-employee")
  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
  public ResponseEntity<DataResponse> insertEmployee(@RequestBody AccountDto accountDto) {
    Account account = accountService.insertAccount(accountDto, UserRole.ROLE_EMPLOYEE);
    if (account != null) {
      return ResponseEntity.status(HttpStatus.OK).body(new DataResponse(true));
    } else {
      DataResponse dataResponse = DataResponse.BAD_REQUEST;
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(dataResponse);
    }
  }

  @PostMapping("/account/add-admin")
  // '//' for the below code to add admin account to test if you want
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<DataResponse> insertAdmin(@RequestBody AccountDto accountDto) {
    Account account = accountService.insertAccount(accountDto, UserRole.ROLE_ADMIN);
    if (account != null) {
      return ResponseEntity.status(HttpStatus.OK).body(new DataResponse(true));
    } else {
      DataResponse dataResponse = DataResponse.BAD_REQUEST;
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(dataResponse);
    }
  }
}
