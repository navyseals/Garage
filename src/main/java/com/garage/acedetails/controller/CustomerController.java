package com.garage.acedetails.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.garage.acedetails.constants.ApplicationConstants;
import com.garage.acedetails.entity.Customer;
import com.garage.acedetails.model.DataResponse;
import com.garage.acedetails.service.CustomerService;

@RequestMapping("/api/v1")
@RestController
public class CustomerController {
  @Autowired
  private CustomerService customerService;

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @ResponseStatus(code = HttpStatus.OK)
  @PostMapping("/customer/add")
  public DataResponse addCustomer(@Valid @RequestBody Customer customer) {
    customerService.insertCustomer(customer);
    return new DataResponse(true);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE')")
  @GetMapping("/customer/get/{id}")
  public ResponseEntity<DataResponse> getCustomer(@PathVariable(name = "id", required = true) String strId) {
    Long id = Long.parseLong(strId);
    Optional<Customer> optionalCustomer = customerService.findByID(id);
    if (optionalCustomer.isPresent()) {
      DataResponse dataResponse = new DataResponse(optionalCustomer.get());
      return ResponseEntity.status(HttpStatus.OK).body(dataResponse);
    } else {
      DataResponse dataResponse = DataResponse.BAD_REQUEST;
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(dataResponse);
    }
  }

  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_EMPLOYEE')")
  @GetMapping("/customer/getAll/{index}")
  public ResponseEntity<DataResponse> getAllCustomer(@PathVariable(name = "index", required = true) String strIndex) {

    int index = Integer.parseInt(strIndex);
    Page<Customer> pageCustomer = customerService.findAll(index, ApplicationConstants.PAGE_SIZE_OF_GET_ALL_CUSTOMER);
    // index from 0 -> (totalPages - 1)
    if ((index <= (pageCustomer.getTotalPages() - 1) && pageCustomer.hasContent())) {
      List<Customer> listCustomer = pageCustomer.getContent();
      DataResponse dataResponse = new DataResponse(listCustomer);
      return ResponseEntity.status(HttpStatus.OK).body(dataResponse);
    } else {
      DataResponse dataResponse = DataResponse.BAD_REQUEST;
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(dataResponse);
    }
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @ResponseStatus(code = HttpStatus.OK)
  @DeleteMapping("/customer/delete/{id}")
  public DataResponse deleteCustomer(@PathVariable(name = "id", required = true) String strId) {
    Long id = Long.parseLong(strId);
    customerService.deleteCustomer(id);
    return new DataResponse(true);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_EMPLOYEE')")
  @PutMapping("/customer/update")
  public ResponseEntity<DataResponse> deleteCustomer(@RequestBody Customer customer) {
    DataResponse dataResponse = new DataResponse(true);
    if (customerService.updateCustomer(customer) == true) {
      return ResponseEntity.status(HttpStatus.OK).body(dataResponse);
    } else {
      dataResponse = DataResponse.BAD_REQUEST;
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(dataResponse);
    }
  }
}
