package com.garage.acedetails.controller;

import com.garage.acedetails.entity.Employee;
import com.garage.acedetails.service.EmployeeService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class EmployeeController {

  @Autowired
  private EmployeeService employeeService;

  @GetMapping("/employees")
  @PreAuthorize("hasAnyAuthority('ROLE_EMPLOYEE', 'ROLE_ADMIN')")
  public ResponseEntity<List<Employee>> getEmployees() {
    return ResponseEntity.ok().body(employeeService.findAll());
  }

  @GetMapping("/employee/{id}")
  @PreAuthorize("hasAnyAuthority('ROLE_EMPLOYEE', 'ROLE_ADMIN')")
  public ResponseEntity<Employee> getEmployeeById(@PathVariable Long id) {
    Optional<Employee> optionalEmployee = employeeService.findById(id);
    if (optionalEmployee.isPresent()) {
      return ResponseEntity.ok().body(optionalEmployee.get());
    }
    return ResponseEntity.notFound().build();
  }

  @PostMapping("/employee")
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<?> insertEmployee(@RequestBody Employee employee) {
    employeeService.insertEmployee(employee);
    return ResponseEntity.ok().build();
  }

  @PutMapping("/employee/{id}")
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<?> updateEmployee(@RequestBody Employee employee, @PathVariable Long id) {
    employee.setEmpID(id);
    employeeService.updateEmployee(employee);
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("/employee/{id}")
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public ResponseEntity<?> deleteEmployee(@PathVariable Long id) {
    employeeService.deleteEmployee(id);
    return ResponseEntity.ok().build();
  }
}
