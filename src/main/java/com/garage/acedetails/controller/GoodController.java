package com.garage.acedetails.controller;


import com.garage.acedetails.entity.Goods;
import com.garage.acedetails.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class GoodController {
    @Autowired
    private GoodsService goodsService;

    @GetMapping("/goodslist")
    public ResponseEntity<List<Goods>> findAll() {
        return ResponseEntity.ok().body(goodsService.findAll());
    }

    @GetMapping("/goods/{goodsId}")
    public ResponseEntity<Goods> findById(@PathVariable long goodsId) {
        return ResponseEntity.ok().body(goodsService
                .findById(goodsId)
                .orElseThrow(() -> new IllegalArgumentException(String.format("Goods id %x does not exists", goodsId))));
    }

    @PostMapping("/goods")
    public ResponseEntity<?> insertGoods(@RequestBody Goods goods) {
        goodsService.insertGoods(goods);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/goods/{goodsId}")
    public ResponseEntity<?> updateGoods(@RequestBody Goods goods, @PathVariable long goodsId) {
        goodsService.updateGoods(goodsId, goods);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/goods/{goodsId}")
    public ResponseEntity<?> deleteGoods(@PathVariable long goodsId) {
        goodsService.deleteGoods(goodsId);
        return ResponseEntity.ok().build();
    }
}
