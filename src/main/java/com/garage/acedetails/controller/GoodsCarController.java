package com.garage.acedetails.controller;

import com.garage.acedetails.entity.GoodsCar;
import com.garage.acedetails.service.GoodsCarService;
import com.garage.acedetails.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class GoodsCarController {
    @Autowired
    private GoodsCarService goodsCarService;

    @Autowired
    private GoodsService goodsService;

    @GetMapping("/goodscars")
    public ResponseEntity<List<GoodsCar>> findAll() {
        return ResponseEntity.ok().body(goodsCarService.findAll());
    }

//    @GetMapping("/goodscar")
//    public ResponseEntity<GoodsCar> findById(@RequestParam("goodsId") long goodsId, @RequestParam("carId") long carId) {
//        return ResponseEntity.ok().body(goodsCarService.findById(new GoodsCarPK(goodsId, carId)))
//    }

    @PostMapping("/goodscar")
    public ResponseEntity<?> insertGoodsCar() {
        return null;
    }

}
