package com.garage.acedetails.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.garage.acedetails.constants.ApplicationConstants;
import com.garage.acedetails.entity.Goods;
import com.garage.acedetails.entity.Image;
import com.garage.acedetails.model.DataResponse;
import com.garage.acedetails.service.GoodsService;
import com.garage.acedetails.service.ImageService;
import com.garage.acedetails.util.ValidId;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
@Validated
public class GoodsController {
  private final GoodsService goodsService;
  private final ImageService imageService;
  private final ObjectMapper objectMapper;

  @PostMapping("/add-goods")
  @ResponseStatus(HttpStatus.OK)
  @Transactional
  @PreAuthorize("hasAnyAuthority('ROLE_EMPLOYEE', 'ROLE_ADMIN')")
  public DataResponse addGoods(@RequestPart("goods") @Valid Goods goods, @RequestParam("files") List<MultipartFile> files) {
    //TODO Rollback transaction when errors occur
    //Note: Parameter files is compulsory
    
    Goods goodsDb = goodsService.insertGoods(goods);
    goodsDb.setListOfImage( new ArrayList<Image>() );
    System.out.println(files == null);
    files.forEach(file->{
      String fileName = imageService.saveToDirectory(file);
      Image image = imageService.saveToDatabase(new Image(null, fileName, goods));
      goodsDb.getListOfImage().add(image);
    });
    JsonNode node = objectMapper.valueToTree(goodsDb);
    ArrayNode imagesNode = objectMapper.valueToTree(goodsDb.getListOfImage());
    ((ObjectNode)node).put("images", imagesNode);
    return new DataResponse(node); 
  }

  @GetMapping("/goods/{id}")
  @ResponseStatus(HttpStatus.OK)
  public DataResponse getGoodsById(@PathVariable("id") @ValidId String strId) {
    Long id = Long.parseLong(strId);
    Optional<Goods> goodsOptional = goodsService.findById(id.longValue());
    if(!goodsOptional.isPresent()) {
      throw new RuntimeException(ApplicationConstants.GOODS_NOT_FOUND);
    }
    Goods goods = goodsOptional.get();
    JsonNode node = objectMapper.valueToTree(goods);
    ArrayNode imagesNode = objectMapper.valueToTree(goods.getListOfImage());
    ((ObjectNode)node).put("images", imagesNode);
    return new DataResponse(node);
  }
}
