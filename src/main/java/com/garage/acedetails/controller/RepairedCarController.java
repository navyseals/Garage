package com.garage.acedetails.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.garage.acedetails.constants.ApplicationConstants;
import com.garage.acedetails.entity.Bill;
import com.garage.acedetails.entity.CarCare;
import com.garage.acedetails.entity.Customer;
import com.garage.acedetails.entity.Goods;
import com.garage.acedetails.entity.RepairedCar;

import com.garage.acedetails.model.DataResponse;
import com.garage.acedetails.service.CustomerService;
import com.garage.acedetails.service.RepairedCarService;
import com.garage.acedetails.util.ValidId;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/v1")
@RestController
@RequiredArgsConstructor
@Validated
public class RepairedCarController {

  private final CustomerService customerService;
  private final RepairedCarService repairedCarService;
  private final ObjectMapper objectMapper;

  @PostMapping("/add-customer-and-car")
  @ResponseStatus(HttpStatus.OK)
  @Transactional //TODO add exception class to rollback, handle exception perclass
  public DataResponse addCustomerAndCar(@RequestBody Map<String, Object> body) {
    //TODO Valid request body
    Customer c = objectMapper.convertValue(body.get("customer"), Customer.class);
    RepairedCar repairedCar = objectMapper.convertValue(body.get("repairedCar"), RepairedCar.class);
    Long[] idServices = objectMapper.convertValue(body.get("idServices"), Long[].class);
    repairedCar.setCustomer(c);
    customerService.insertCustomer(c);
    Long idRepairedCar = repairedCarService.addRepairedCar(repairedCar).getId();

    //TODO throw exception then rollback
    for (Long idService : idServices) {
      repairedCarService.addServiceToCar(idRepairedCar, idService);
    }
    return new DataResponse(repairedCar);
  }

  @GetMapping("/car/{id}")
  @ResponseStatus(HttpStatus.OK)
  public DataResponse getRepairedCarById(@PathVariable("id") @ValidId String strId) {
    Long id = Long.parseLong(strId);
    Map<String, Object> result = new HashMap<>();
    RepairedCar repairedCar = repairedCarService.findRepairedCarById(id);
    Customer customer = repairedCar.getCustomer();
    Bill bill = repairedCar.getBill();

    CarCare[] usedService = repairedCar.getRepairedCarService()
        .stream()
        .map(item -> {
          CarCare carService = item.getCarCare();
          carService.setCarCarePrice(item.getSerPrice()); //Make sure
          return carService;
        })
        .toArray(CarCare[]::new);
    Goods[] goods = repairedCar.getGoodsCars()
        .stream()
        .map(item -> {
          return item.getGoodsCarPK().getGoods();
        })
        .toArray(Goods[]::new);
    result.put("repairedCar", repairedCar);
    result.put("customer", customer);
    result.put("bill", bill);
    result.put("usedServices", usedService);
    result.put("goods", goods);
    return new DataResponse(result);

//		RepairedCar repairedCar = repairedCarService.findRepairedCarById(id);
//		Customer customer = repairedCar.getCustomer();
//		Bill bill = repairedCar.getBill();
//
//		CarService[] usedService = repairedCar.getRepairedCarService()
//									.stream()
//									.map( item->{
//										CarService carService = item.getService();
//										carService.setSerPrice( item.getSerPrice() ); //Make sure 
//										return carService;
//									} )
//									.toArray(CarService[]::new);
//		Goods[] goods = repairedCar.getGoodsCars()
//									.stream()
//									.map(item->{
//										return item.getGoodsCarPK().getGoods();
//									})
//									.toArray(Goods[]::new);
//		JsonNode res = objectMapper.valueToTree(repairedCar);
//		JsonNode cusNode = objectMapper.valueToTree(customer);
//		((ObjectNode)res).put("customer", cusNode);
//		ArrayNode carServiceNode = objectMapper.valueToTree(usedService);
//		((ObjectNode)res).put("usedServices", carServiceNode);
//		return ResponseEntity.ok().body(res);
  }

  @GetMapping("/car")
  @ResponseStatus(HttpStatus.OK)
  public DataResponse getRepairedCars(@RequestParam(defaultValue = "1", name = "page") String strPage) {
    int page = 0;
    try {
      page = Integer.parseInt(strPage);
    } catch (NumberFormatException e) {
      e.printStackTrace();
      throw new NumberFormatException("Page is not valid");
    }
    page--;
    page = page < 0 ? 0 : page;
    Page<RepairedCar> repairedCarPage = repairedCarService.findRepairedCarWithPage(page);
    Map<String, Object> result = new HashMap<>();
    result.put("repairedCars", repairedCarPage.toList());
    result.put("maxPage", repairedCarPage.getTotalPages());
    result.put("page", ++page);
    return new DataResponse(result);
  }

  @DeleteMapping("/delete-car/{id}")
  @ResponseStatus(HttpStatus.OK)
  public DataResponse deleteRepairedCarById(@PathVariable("id") @ValidId String strId) {
    Long id = Long.parseLong(strId);
    repairedCarService.deleteRepairedCarById(id);
    ObjectNode node = objectMapper.createObjectNode();
    node.put("statusCode", HttpStatus.OK.value());
    node.put("message", "successful");
    return new DataResponse(true);
  }

  @PutMapping("/update-car/{id}")
  @ResponseStatus(HttpStatus.OK)
  public DataResponse updateRepairedCarById(@PathVariable("id") @ValidId String strId,
      @RequestBody RepairedCar repairedCar) {
    Long id = Long.parseLong(strId);
    RepairedCar result = repairedCarService.updateRepairedCar(id, repairedCar);
    return new DataResponse(result);
  }
}
