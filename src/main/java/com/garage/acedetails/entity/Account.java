package com.garage.acedetails.entity;


import com.garage.acedetails.util.UserRole;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Table(name = "account")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", columnDefinition = "BIGINT(19)")
  private Long id;

  @Column(name = "acc_username", columnDefinition = "VARCHAR(50)", unique = true, nullable = false)
  private String accUsername;

  @Column(name = "acc_password", columnDefinition = "VARCHAR(255)", nullable = false)
  private String accPassword;

  @Column(name = "acc_role", nullable = false)
  @Enumerated(EnumType.STRING)
  private UserRole accRole;
}
