package com.garage.acedetails.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "bills")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Bill {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "bill_number", columnDefinition = "BIGINT(19)")
  private Long billNumber;

  @Column(name = "bill_method", columnDefinition = "NVARCHAR(50)")
  private String billMethod;

  @Column(name = "bill_amount", columnDefinition = "BIGINT(19)", nullable = false)
  private Long billAmount;

  @Column(name = "bill_date", columnDefinition = "DATE", nullable = false)
  @Temporal(TemporalType.DATE)
  private Date billDate;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "repaired_car_id", nullable = false)
  private RepairedCar repairedCar;

}
