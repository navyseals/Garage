package com.garage.acedetails.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class CarCare {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long carCareNumber;

  @NotNull(message = "Service name must not be null")
  @NotBlank(message = "Service name must not be blank")
  @Column(nullable = false, unique = true)
  private String carCareName;

  private double carCarePrice;
  private String carCareDescription;

  @OneToMany(mappedBy = "carCare", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JsonIgnore
  private Set<UsedService> setUsedService;



  public CarCare(
      @NotNull(message = "Service name must not be null") @NotBlank(
          message = "Service name must not be blank") String carCareName,
      double carCarePrice, String carCareDescription) {
    super();
    this.carCareName = carCareName;
    this.carCarePrice = carCarePrice;
    this.carCareDescription = carCareDescription;
  }


}
