package com.garage.acedetails.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "repairedCars")
@Data
@Entity
@Table(name = "customer")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Customer {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "cus_id", columnDefinition = "BIGINT(19)")
  private Long cusID;

  @Column(name = "cus_first_name", columnDefinition = "NVARCHAR(50)", nullable = false)
  private String cusFirstName;

  @Column(name = "cus_last_name", columnDefinition = "NVARCHAR(50)")
  private String cusLastName;

  @Column(name = "cus_address", columnDefinition = "NVARCHAR(255)")
  private String cusAddress;

  @Column(name = "cus_phone", columnDefinition = "VARCHAR(12)", nullable = false, unique = true)
  private String cusPhone;

  @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JsonIgnore
  private List<RepairedCar> repairedCars;

  @OneToOne()
  @JoinColumn(name = "acc_id", nullable = true, unique = true)
  @JsonIgnore
  private Account account;
}
