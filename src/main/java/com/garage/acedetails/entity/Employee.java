package com.garage.acedetails.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "employee")
public class Employee {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "emp_id", columnDefinition = "BIGINT(19)")
  private Long empID;

  @Column(name = "emp_first_name", columnDefinition = "NVARCHAR(50)", nullable = false)
  private String empFirstName;

  @Column(name = "emp_last_name", columnDefinition = "NVARCHAR(50)")
  private String empLastName;

  @Column(name = "emp_address", columnDefinition = "NVARCHAR(255)")
  private String empAddress;

  @Column(name = "emp_phone", columnDefinition = "VARCHAR(12)", nullable = false, unique = true)
  private String empPhone;

  @Column(name = "emp_salary", columnDefinition = "DOUBLE", nullable = false)
  private double empSalary;

  @Column(name = "emp_allowance", columnDefinition = "DOUBLE")
  private double empAllowance;

  @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "acc_id", nullable = false, unique = true)
  private Account account;


}
