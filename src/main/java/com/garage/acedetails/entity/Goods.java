package com.garage.acedetails.entity;

import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Goods")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Goods {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "goods_number")
  private Long goodsNumber;

  @NotNull(message = "Goods name mustn't be null")
  @NotBlank(message = "Goods name mustn't be blank")
  @Column(name = "goods_name", columnDefinition = "NVARCHAR(80)", nullable = false)
  private String goodsName;

  
  @Column(name = "goods_price", nullable = false)
  private double goodsPrice;

  @Column(name = "goods_total_quantity", nullable = false)
  private int goodsTotalQuantity;


  @Column(name = "goods_description", columnDefinition = "TEXT", nullable = false)
  private String goodsDescription;

  @ManyToOne
  @JoinColumn(name = "cat_number")
  private Category category;

  @OneToMany(mappedBy = "goodsCarPK.goods", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JsonIgnore
  private Set<GoodsCar> listOfGoodsCar;

  @OneToMany(mappedBy = "goods", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JsonIgnore
  private List<Image> listOfImage;

}
