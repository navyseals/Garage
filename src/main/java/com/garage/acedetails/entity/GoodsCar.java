package com.garage.acedetails.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "goods_Car")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GoodsCar {

  @EmbeddedId
  private GoodsCarPK goodsCarPK;

  @Column(name = "goods_car_quantity", nullable = false)
  private int goodsCarQuantity;

  @Column(name = "goods_car_totalprice", nullable = false)
  private double goodsCarTotalPrice;

  @Column(name = "goods_car_price", nullable = false)
  private double goodsCarPrice;

  public GoodsCar(int goodsCarQuantity, double goodsCarTotalPrice, double goodsCarPrice) {
    this.goodsCarQuantity = goodsCarQuantity;
    this.goodsCarTotalPrice = goodsCarTotalPrice;
    this.goodsCarPrice = goodsCarPrice;
  }
}
