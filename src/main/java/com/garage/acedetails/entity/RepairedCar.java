package com.garage.acedetails.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
//@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class RepairedCar {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "start_date")
  private LocalDateTime startDate;

  @Column(name = "end_date")
  private LocalDateTime endDate;

  @Column(name = "description")
  private String description;

  @Column(name = "type")
  @NotBlank(message = "Car type must not blank")
  private String type;

  @Column(name = "car_number")
  private String carNumber;

  @Column(name = "car_status")
  private String carStatus;

  @OneToMany(mappedBy = "repairedCar", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JsonIgnore
  private Set<UsedService> repairedCarService;


  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "cus_id", nullable = false)
  @JsonIgnore
  private Customer customer;


  @OneToOne(mappedBy = "repairedCar", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JsonIgnore
  private Bill bill;


  @OneToMany(mappedBy = "goodsCarPK.repairedCar", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JsonIgnore
  private Set<GoodsCar> goodsCars;


  public RepairedCar(LocalDateTime reCarStartingDate, String reCarDescription, String reCarType, String reCarNumber,
      String reCarStatus) {
    super();
    this.startDate = reCarStartingDate;
    this.description = reCarDescription;
    this.type = reCarType;
    this.carNumber = reCarNumber;
    this.carStatus = reCarStatus;
  }


  @Override
  public String toString() {
    return "RepairedCar [id=" + id + ", startDate=" + startDate + ", endDate="
        + endDate + ", description=" + description + ", type=" + type
        + ", carNumber=" + carNumber + ", carStatus=" + carStatus + "]";
  }

}
