package com.garage.acedetails.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class UsedService {

  @EmbeddedId
  private CompositePK id = new CompositePK();

  @ManyToOne(fetch = FetchType.LAZY)
  @MapsId("id")
  @JoinColumn(name = "repaired_car_id")
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  private RepairedCar repairedCar;

  @ManyToOne(fetch = FetchType.LAZY)
  @MapsId("car_care_number")
  @JoinColumn(name = "car_care_number")
  @JsonIgnore
  @EqualsAndHashCode.Exclude
  private CarCare carCare;

  private double serPrice;

  public UsedService(CompositePK id, double serPrice) {
    super();
    this.id = id;
    this.serPrice = serPrice;
  }

  @Embeddable
  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class CompositePK implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "repaired_car_id")
    private Long reCarId;
    @Column(name = "car_care_number")
    private Long serNumber;
  }
}
