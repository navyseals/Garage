package com.garage.acedetails.exception;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import com.garage.acedetails.constants.ApplicationConstants;
import com.garage.acedetails.model.DataResponse;


@RestControllerAdvice
@ResponseStatus
public class RestResponseEntityExceptionHandler {

  @ExceptionHandler(RuntimeException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public DataResponse generalException(RuntimeException exception) {
    return new DataResponse(ApplicationConstants.BAD_REQUEST, exception.getMessage(), null, HttpStatus.BAD_REQUEST.value());
  }

  @ExceptionHandler(NumberFormatException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public DataResponse badRequestNumberFormatException(NumberFormatException exception,
      WebRequest request) {
    return new DataResponse(exception.getMessage(), exception.getMessage(), null, HttpStatus.BAD_REQUEST.value());
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public DataResponse failValidRequestBody(MethodArgumentNotValidException exception, WebRequest request) {
    String errors = "";
    String fieldName = "";
    String errorMessage = "";
    for(ObjectError error: exception.getBindingResult().getAllErrors()) {
      fieldName = ((FieldError) error).getField();
      errorMessage = error.getDefaultMessage();
      errors += fieldName + ": " + errorMessage + "\n";
    }
    return new DataResponse(errors, errors, null, HttpStatus.BAD_REQUEST.value());
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
  public DataResponse sqlConstraintViolationException(SQLIntegrityConstraintViolationException exception,
      WebRequest request) {
    return new DataResponse(exception.getMessage(), exception.getMessage(), null, HttpStatus.BAD_REQUEST.value());
  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(ConstraintViolationException.class)
  public DataResponse idInvalidException(ConstraintViolationException exception, WebRequest request) {
    return new DataResponse(exception.getMessage(), exception.getMessage(), null, HttpStatus.BAD_REQUEST.value());
  }

  //Exception when delete nonexisted object
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(EmptyResultDataAccessException.class)
  public DataResponse emptyResultDataAccessException(EmptyResultDataAccessException exception, WebRequest request) {
    return new DataResponse(exception.getMessage(), exception.getMessage(), null, HttpStatus.BAD_REQUEST.value());
  }

}
