package com.garage.acedetails.repository;

import com.garage.acedetails.entity.CarCare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarCareRepository extends JpaRepository<CarCare, Long> {

  public CarCare findByCarCareName(String serName);
}
