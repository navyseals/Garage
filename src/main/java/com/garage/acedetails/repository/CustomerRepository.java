package com.garage.acedetails.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.garage.acedetails.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
