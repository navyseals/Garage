package com.garage.acedetails.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.garage.acedetails.dto.AccountDto;
import com.garage.acedetails.entity.Account;
import com.garage.acedetails.util.UserRole;

@Service
public interface AccountService {
  public List<Account> findAll();

  Account insertAccount(AccountDto accountDto, UserRole accountRole);
}
