package com.garage.acedetails.service;

import com.garage.acedetails.entity.Bill;
import java.util.List;
import java.util.Optional;

public interface BillService {

  void insertCustomer(Bill bill);

  List<Bill> findAll();

  Optional<Bill> findByID(Long id);

  void deleteCustomer(Long id);

  boolean updateCustomer(Bill bill);

}