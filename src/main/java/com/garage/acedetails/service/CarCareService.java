package com.garage.acedetails.service;

import com.garage.acedetails.entity.CarCare;
import java.util.List;

public interface CarCareService {

  public CarCare addCarCare(CarCare service);

  public List<CarCare> findAllCarCare();

  public CarCare findCarCareById(Long id);

  public void deleteCarCare(Long id);

  public CarCare updateCarCare(Long id, CarCare service);
}
