package com.garage.acedetails.service;


import com.garage.acedetails.entity.Category;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public interface CategorySevice {

  List<Category> findAll();

  Optional<Category> findByID(Long id);

  public void insertCategory(Category category);

  public void deleteCategory(Long id);

  public boolean updateCategory(Category category);
}
