package com.garage.acedetails.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import com.garage.acedetails.entity.Customer;

public interface CustomerService {

  Customer insertCustomer(Customer customer);

  Page<Customer> findAll(int pageIndex, int pageSize);

  Optional<Customer> findByID(Long id);

  void deleteCustomer(Long id);

  boolean updateCustomer(Customer customer);
}
