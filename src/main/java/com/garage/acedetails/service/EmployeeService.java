package com.garage.acedetails.service;

import com.garage.acedetails.entity.Employee;
import java.util.List;
import java.util.Optional;


public interface EmployeeService {

  Optional<Employee> findById(Long id);

  List<Employee> findAll();

  void deleteEmployee(Long id);

  Employee insertEmployee(Employee employee);

  boolean updateEmployee(Employee employee);

}
