package com.garage.acedetails.service;

import com.garage.acedetails.entity.GoodsCar;
import com.garage.acedetails.entity.GoodsCarPK;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public interface GoodsCarService {

  List<GoodsCar> findAll();

  Optional<GoodsCar> findById(GoodsCarPK goodsCarPK);

  boolean insertGoodsCar(GoodsCar goodsCar);

  boolean updateGoodsCar(GoodsCarPK goodsCarPK, GoodsCar goodsCar);

  void deleteGoodsCar(GoodsCarPK goodsCarPK);
}
