package com.garage.acedetails.service;

import com.garage.acedetails.entity.Goods;
import java.util.List;
import java.util.Optional;

public interface GoodsService {

  public List<Goods> findAll();

  public Optional<Goods> findById(long id);

  public Goods insertGoods(Goods goods);

  public boolean updateGoods(long id, Goods goods);

  public void deleteGoods(long id);
}
