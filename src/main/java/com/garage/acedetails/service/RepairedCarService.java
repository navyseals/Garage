package com.garage.acedetails.service;


import com.garage.acedetails.entity.RepairedCar;
import java.util.List;
import org.springframework.data.domain.Page;


public interface RepairedCarService {

  public RepairedCar addRepairedCar(RepairedCar repairedCar);

  public List<RepairedCar> findAllRepairedCar();

  public RepairedCar findRepairedCarById(Long id);

  public void deleteRepairedCarById(Long id);

  public RepairedCar updateRepairedCar(Long id, RepairedCar repairedCar);

  public void addServiceToCar(Long repairedCarId, Long carServiceId);

  public Page<RepairedCar> findRepairedCarWithPage(int page);
}
