package com.garage.acedetails.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.garage.acedetails.dto.AccountDto;
import com.garage.acedetails.entity.Account;
import com.garage.acedetails.repository.AccountRepository;
import com.garage.acedetails.service.AccountService;
import com.garage.acedetails.util.UserRole;

@Service
public class AccountServiceImpl implements AccountService {
  @Autowired
  private AccountRepository accountRepository;
  @Autowired
  private PasswordEncoder passwordEncoder;

  @Override
  public List<Account> findAll() {
    return null;
  }

  @Override
  public Account insertAccount(AccountDto accountDto, UserRole accountRole) {
    if (accountDto.getPassword().equals(accountDto.getConfirmPassword()) == true) {
      Account account = new Account();
      // Encode password by PasswordEncoder of spring security
      String encoderPassword = passwordEncoder.encode(accountDto.getPassword().toString());
      account.setAccPassword(encoderPassword);
      account.setAccUsername(accountDto.getUsername().toString());
      account.setAccRole(accountRole);
      return accountRepository.save(account);
    } else {
      return null; // if password and confirm password not same => return null
    }

  }

}
