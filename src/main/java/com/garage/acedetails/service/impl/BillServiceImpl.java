package com.garage.acedetails.service.impl;

import com.garage.acedetails.entity.Bill;
import com.garage.acedetails.repository.BillRepository;
import com.garage.acedetails.service.BillService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BillServiceImpl implements BillService {

  @Autowired
  private BillRepository billRepository;

  @Override
  public void insertCustomer(Bill bill) {
    billRepository.save(bill);
  }

  @Override
  public List<Bill> findAll() {
    return billRepository.findAll();
  }

  @Override
  public Optional<Bill> findByID(Long id) {
    return billRepository.findById(id);
  }

  @Override
  public void deleteCustomer(Long id) {
    billRepository.deleteById(id);
  }

  @Override
  public boolean updateCustomer(Bill bill) {
    Optional<Bill> originalBill = billRepository.findById(bill.getBillNumber());
    if (originalBill.isPresent()) {
      billRepository.save(bill);
      return true;
    }
    return false;
  }
}
