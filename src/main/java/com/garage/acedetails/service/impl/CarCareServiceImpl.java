package com.garage.acedetails.service.impl;

import com.garage.acedetails.constants.ApplicationConstants;
import com.garage.acedetails.entity.CarCare;
import com.garage.acedetails.repository.CarCareRepository;
import com.garage.acedetails.service.CarCareService;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CarCareServiceImpl implements CarCareService {

  private final CarCareRepository carCareRepository;

  @Override
  public CarCare addCarCare(CarCare service) {
    String serName = service.getCarCareName();
    if (carCareRepository.findByCarCareName(serName) != null) {
      throw new RuntimeException(ApplicationConstants.CAR_SERVICE_NAME_IS_DUPLICATED);
    }
    return carCareRepository.save(service);
  }

  @Override
  public List<CarCare> findAllCarCare() {
    return carCareRepository.findAll();
  }

  @Override
  public CarCare findCarCareById(Long id) {
    Optional<CarCare> carService = carCareRepository.findById(id);
    if (!carService.isPresent()) {
      throw new RuntimeException(ApplicationConstants.SERVICE_NOT_FOUND);
    }
    return carService.get();
  }

  @Override
  public void deleteCarCare(Long id) {
    carCareRepository.deleteById(id);
  }

  @Override
  public CarCare updateCarCare(Long id, CarCare carCare) {
    Optional<CarCare> optionalCarServiceDB = carCareRepository.findById(id);
    if (!optionalCarServiceDB.isPresent()) {
      throw new RuntimeException(ApplicationConstants.CAR_NOT_FOUND);
    }
    CarCare carCareDB = optionalCarServiceDB.get();
    if (carCare.getCarCareName() != null && !carCare.getCarCareName().trim().equals("")) {
      carCareDB.setCarCareName(carCare.getCarCareName());
    }
    if (carCare.getCarCareDescription() != null && !carCare.getCarCareDescription().trim().equals("")) {
      carCareDB.setCarCareDescription(carCare.getCarCareDescription());
    }
    if (carCare.getCarCarePrice() <= 0) {
      //TODO throw price exception
    } else {
      carCareDB.setCarCarePrice(carCare.getCarCarePrice());
    }
    return carCareRepository.save(carCareDB);
  }


}
