package com.garage.acedetails.service.impl;

import com.garage.acedetails.entity.Category;
import com.garage.acedetails.repository.CategoryRepository;
import com.garage.acedetails.service.CategorySevice;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;

public class CategorySeviceimpl implements CategorySevice {

  @Autowired
  private CategoryRepository categoryRepository;

  @Override
  public void insertCategory(Category category) {
    // TODO Auto-generated method stub
    categoryRepository.save(category);
  }

  @Override
  public void deleteCategory(Long id) {
    // TODO Auto-generated method stub
    categoryRepository.deleteById(id);
  }

  @Override
  public List<Category> findAll() {
    return categoryRepository.findAll();
  }

  @Override
  public Optional<Category> findByID(Long id) {
    // TODO Auto-generated method stub
    return categoryRepository.findById(id);
  }

  @Override
  public boolean updateCategory(Category category) {
    Optional<Category> origiCategory = categoryRepository.findById(category.getCatNumber());
    if (origiCategory.isPresent()) {
      categoryRepository.save(category);
      return true;
    }
    return false;
  }

}
