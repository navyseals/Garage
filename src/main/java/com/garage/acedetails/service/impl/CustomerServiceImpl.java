package com.garage.acedetails.service.impl;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.garage.acedetails.entity.Customer;
import com.garage.acedetails.repository.CustomerRepository;
import com.garage.acedetails.service.CustomerService;


@Service
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private CustomerRepository customerRepository;

  public Customer insertCustomer(Customer customer) {
    return customerRepository.save(customer);
  }

  public Page<Customer> findAll(int pageIndex, int pageSize) {
    Pageable pageable = PageRequest.of(pageIndex, pageSize);
    return customerRepository.findAll(pageable);
  }

  public Optional<Customer> findByID(Long id) {
    return customerRepository.findById(id);
  }

  public void deleteCustomer(Long id) {
    customerRepository.deleteById(id);
  }

  public boolean updateCustomer(Customer customer) {
    Optional<Customer> originalCustomer = customerRepository.findById(customer.getCusID());
    if (originalCustomer.isPresent()) {
      customerRepository.save(customer);
      return true;
    } else {
      return false;
    }
  }
}
