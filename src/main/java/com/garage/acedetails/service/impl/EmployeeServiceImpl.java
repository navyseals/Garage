package com.garage.acedetails.service.impl;

import com.garage.acedetails.entity.Employee;
import com.garage.acedetails.repository.EmployeeRepository;
import com.garage.acedetails.service.EmployeeService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {

  @Autowired
  private EmployeeRepository employeeRepository;

  @Override
  public Optional<Employee> findById(Long id) {
    return employeeRepository.findById(id);
  }

  @Override
  public List<Employee> findAll() {
    return employeeRepository.findAll();
  }

  @Override
  public void deleteEmployee(Long id) {
    employeeRepository.deleteById(id);
  }

  @Override
  public Employee insertEmployee(Employee employee) {
    // employee.setEmpPassword(passwordEncoder.encode(employee.getEmpPassword()));
    // Encode o Account
    return employeeRepository.save(employee);
  }

  @Override
  public boolean updateEmployee(Employee employee) {
    Optional<Employee> originalEmployee = employeeRepository.findById(employee.getEmpID());
    if (originalEmployee.isPresent()) {
      employeeRepository.save(employee);
      return true;
    } else {
      return false;
    }
  }

}
