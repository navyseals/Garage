package com.garage.acedetails.service.impl;

import com.garage.acedetails.entity.GoodsCar;
import com.garage.acedetails.entity.GoodsCarPK;
import com.garage.acedetails.repository.GoodsCarRepository;
import com.garage.acedetails.service.GoodsCarService;
import com.garage.acedetails.util.GoodsCarValidator;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoodsCarServiceImpl implements GoodsCarService {

  @Autowired
  private GoodsCarRepository goodsCarRepository;

  @Override
  public List<GoodsCar> findAll() {
    return goodsCarRepository.findAll();
  }

  @Override
  public Optional<GoodsCar> findById(GoodsCarPK goodsCarPK) {
    return goodsCarRepository.findById(goodsCarPK);
  }

  @Override
  public boolean insertGoodsCar(GoodsCar goodsCar) {
    if (GoodsCarValidator.isValid(goodsCar)) {
      goodsCarRepository.save(goodsCar);
      return true;
    } else {
      System.out.println("Goods Car not valid");
    }
    return false;
  }

  @Override
  public boolean updateGoodsCar(GoodsCarPK goodsCarPK, GoodsCar goodsCar) {
    Optional<GoodsCar> optionalGoodsCar = goodsCarRepository.findById(goodsCarPK);
    if (GoodsCarValidator.isValid(goodsCar)) {
      if (optionalGoodsCar.isPresent()) {
        GoodsCar goodsCarFromDB = optionalGoodsCar.get();
        goodsCarFromDB.setGoodsCarPrice(goodsCar.getGoodsCarPrice());
        goodsCarFromDB.setGoodsCarQuantity(goodsCar.getGoodsCarQuantity());
        goodsCarFromDB.setGoodsCarTotalPrice(goodsCar.getGoodsCarTotalPrice());
        goodsCarRepository.save(goodsCarFromDB);
        return true;
      } else {
        System.out.println("Goods Car not found");
      }
    } else {
      System.out.println("Goods Car not valid");
    }
    return false;
  }

  @Override
  public void deleteGoodsCar(GoodsCarPK goodsCarPK) {
    goodsCarRepository.deleteById(goodsCarPK);
  }
}
