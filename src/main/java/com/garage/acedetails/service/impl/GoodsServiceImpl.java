package com.garage.acedetails.service.impl;

import com.garage.acedetails.entity.Goods;
import com.garage.acedetails.repository.GoodsRepository;
import com.garage.acedetails.service.GoodsService;
import com.garage.acedetails.util.GoodsValidator;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoodsServiceImpl implements GoodsService {

  @Autowired
  private GoodsRepository goodsRepository;

  public List<Goods> findAll() {
    return goodsRepository.findAll();
  }

  public Optional<Goods> findById(long id) {
    return goodsRepository.findById(id);
  }

  public Goods insertGoods(Goods goods) {
    if (GoodsValidator.isValid(goods)) {
      return goodsRepository.save(goods);
    } else {
      System.out.println("Goods not valid");
    }
    return null;
  }

  public boolean updateGoods(long id, Goods goods) {
    Optional<Goods> optionalGoods = findById(id);
    if (GoodsValidator.isValid(goods)) {
      if (optionalGoods.isPresent()) {
        Goods goodsFromDB = optionalGoods.get();
        goodsFromDB.setGoodsName(goods.getGoodsName());
        goodsFromDB.setGoodsPrice(goods.getGoodsPrice());
        goodsFromDB.setGoodsTotalQuantity(goods.getGoodsTotalQuantity());
//            goodsFromDB.setCategory(goods.getCategory());
        goodsFromDB.setListOfGoodsCar(goods.getListOfGoodsCar());
        goodsRepository.save(goodsFromDB);
        return true;
      } else {
        System.out.println("Goods not found");
      }
    } else {
      System.out.println("Goods not valid");
    }
    return false;
  }

  public void deleteGoods(long id) {
    goodsRepository.deleteById(id);
  }
}
