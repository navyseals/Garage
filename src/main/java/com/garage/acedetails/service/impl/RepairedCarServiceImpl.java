package com.garage.acedetails.service.impl;

import com.garage.acedetails.constants.ApplicationConstants;
import com.garage.acedetails.entity.CarCare;
import com.garage.acedetails.entity.RepairedCar;
import com.garage.acedetails.entity.UsedService;
import com.garage.acedetails.repository.CarCareRepository;
import com.garage.acedetails.repository.RepairedCarRepository;
import com.garage.acedetails.repository.UsedServiceRepository;
import com.garage.acedetails.service.RepairedCarService;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RepairedCarServiceImpl implements RepairedCarService {

  private final RepairedCarRepository repairedCarRepository;
  private final CarCareRepository carCareRepository;
  private final UsedServiceRepository usedServiceRepository;

  @Override
  public RepairedCar addRepairedCar(RepairedCar repairedCar) {
    return repairedCarRepository.save(repairedCar);
  }

  @Override
  public List<RepairedCar> findAllRepairedCar() {
    return repairedCarRepository.findAll();
  }

  @Override
  public RepairedCar findRepairedCarById(Long id) {
    Optional<RepairedCar> repairedCar = repairedCarRepository.findById(id);
    if (!repairedCar.isPresent()) {
      throw new RuntimeException(ApplicationConstants.CAR_NOT_FOUND);
    }
    return repairedCar.get();
  }

  @Override
  public void deleteRepairedCarById(Long id) {
    // TODO Auto-generated method stub
    repairedCarRepository.deleteById(id);
    ;
  }

  @Override
  public RepairedCar updateRepairedCar(Long id, RepairedCar repairedCar) {
    Optional<RepairedCar> optionalCarDB = repairedCarRepository.findById(id);
    if (!optionalCarDB.isPresent()) {
      throw new RuntimeException(ApplicationConstants.CAR_NOT_FOUND);
    }
    RepairedCar carDB = optionalCarDB.get();
		if (repairedCar.getDescription() != null && !repairedCar.getDescription().trim().equals("")) {
			carDB.setDescription(repairedCar.getDescription());
		}
		if (repairedCar.getStartDate() != null) {
			carDB.setStartDate(repairedCar.getStartDate());
		}
    if (repairedCar.getEndDate() != null) {
      if (repairedCar.getEndDate().compareTo(carDB.getStartDate()) < 0) {
        //TODO throw new Exception Date
      }
      carDB.setEndDate(repairedCar.getEndDate());
    }
		if (repairedCar.getCarNumber() != null && !repairedCar.getCarNumber().trim().equals("")) {
			carDB.setCarNumber(repairedCar.getCarNumber());
		}
		if (repairedCar.getCarStatus() != null && !repairedCar.getCarStatus().trim().equals("")) {
			carDB.setCarStatus(repairedCar.getCarStatus());
		}
		if (repairedCar.getType() != null && !repairedCar.getType().trim().equals("")) {
			carDB.setType(repairedCar.getType());
		}
    return repairedCarRepository.save(carDB);

  }

  @Override
  public void addServiceToCar(Long repairedCarId, Long carServiceId) {
    Optional<RepairedCar> optionalRepairedCar = repairedCarRepository.findById(repairedCarId);
    Optional<CarCare> optionalCarService = carCareRepository.findById(carServiceId);
    if (!optionalRepairedCar.isPresent()) {
      throw new RuntimeException(ApplicationConstants.CAR_NOT_FOUND);
    }
    if (!optionalCarService.isPresent()) {
      throw new RuntimeException(ApplicationConstants.SERVICE_NOT_FOUND);
    }

    Optional<UsedService> service = usedServiceRepository.findById(
        new UsedService.CompositePK(repairedCarId, carServiceId));
    if (service.isPresent()) {
      //TODO throw ServiceAlreadyUsed Exception
      System.out.println("service already used");
      return;
    }
    UsedService usedService = new UsedService(new UsedService.CompositePK(repairedCarId, carServiceId),
        optionalCarService.get().getCarCarePrice());
    usedServiceRepository.save(usedService);
  }

  public Page<RepairedCar> findRepairedCarWithPage(int page) {
    return repairedCarRepository.findAll(PageRequest.of(page, 2));
  }
}
